#!/bin/bash

# Author: Alex Richard Ford (arf4188@gmail.com)

THIS_SCRIPT="$(realpath ${0})"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
BEANCOUNT_IONIC_HOME="$(dirname ${SCRIPTS_DIR})"
BEANCOUNT_IONIC_APP_DIR="${BEANCOUNT_IONIC_HOME}/app"


ORIGIN_DIR="$(pwd)"

echo "[ INFO ] Please wait while developing server boots up... this may take awhile."
echo "[ INFO ] A browser will automatically open to the app once it is ready!"
cd "${BEANCOUNT_IONIC_APP_DIR}" &&
  ionic serve

cd "${ORIGIN_DIR}"
