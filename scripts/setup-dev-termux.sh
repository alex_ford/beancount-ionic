#!/bin/bash

#//
# This allows Android Termux to be used for basic development of this project.
#
# NOTE: cordova is not yet figured out, so can't build the native APK.
#
# Author: Alex
#//

echo "[ INFO ] $0 is starting..."

function check_error() {
    if [ $? != 0 ]; then
        echo "[ ERRO ] a problem has occurred!"
        exit 1
    fi
}

pkg upgrade
check_error

pkg i -y openssl
check_error
pkg i -y build-essential
check_error
#pkg i -y python2
#pkg i -y python3
#pkg install python-dev
pkg i clang \
      libiconv-dev \
      libxml2-dev \
      libxslt-dev \
      zlib-dev
check_error
pkg i -y nodejs-lts
check_error

pip install lxml
check_error

npm install -g ionic
check_error

echo "[ INFO ] $0 has finished."
