# Development on Mobile Device

This doc discusses the possibility of developing Beancount Ionic directly on a mobile device. Specifically, it deals with development on Android using a Unix-like environment called Termux.

## Getting Started

1.  Install Termux from the Google Play Store
2.  Open the Termux app
3.  Install `npm` by typing: `pkg install npm`
4.  Install Ionic Framework: `npm install -g ionic`
5.  Clone the Beancount Ionic git repo: `git clone git@gitlab.com:alex_ford/beancount-ionic.git`
6.  Install project dependencies: `cd beancount-ionic && npm install`

## Node-sass

There is a problem with `node-sass` for Android. This will prevent `npm install` from succeeding, as well as `ionic build/serve`.

1. Ensure that nodejs is version 10 and **not** version 12! Install the `nodejs-lts` package
2. Edit the ~/node-sass/.,./common.gypjs file so that android build flags are: `-...`

Now npm install should work!
