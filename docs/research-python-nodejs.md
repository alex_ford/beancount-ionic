# Research Python and NodeJS

The core Beancount software is all written in Python, but the selected Progressive Web App (PWA) technology stack ([Ionic Framework](./ionic-framework.md)) is primarily JavaScript based. This presents a disconnect between these two sides which needs to be resolved. There are several potential options, which are discussed in this doc. There are also many variations of this Python/JavaScript pairing, so it's important to narrow the focus of this document in respects to Beancount Ionic:

-   The target runtime environment must maintain properties of a Progressive Web App (as provided by the Ionic Framework)
-   ...

## Option 1: use NodeJS `child_process` API to invoke Python/Beancount

<https://www.sohamkamani.com/blog/2015/08/21/python-nodejs-comm/>

## Option 2: use `python-shell` to invoke Python/Beancount

<https://www.npmjs.com/package/python-shell>

## Option 3: use Transcrypt

<https://www.transcrypt.org/>

-   The Transcrypt project allows you to transpile Python code into JavaScript.
-   Unfortunately, since our starting point for runtime execution begins in JavaScript (with Ionic Framework), it might be tricky to leverage Transcrypt.
-   Perhaps it's possible to use Transcrypt to transpile all of Beancount into a JavaScript version? (Need to explore this!)

## Option 4: use Pyjs

<http://pyjs.org/>

-   Similar to Transcrypt?
-   Perhaps an alternative to transpiling a Beancount version into JavaScript?

## Option 5: use Brython

<https://brython.info/>

-   A Python execution runtime for the browser! The website states: "Brython is designed to replace JavaScript"
