<#
    Windows PowerShell script for lauching Visual Studio Code in an
    isolated/independent environment. This means that the user data
    and extensions are separate from the main user instance that is
    normally launched when you simply type 'code'.
    
    You can open files in this specific isolated environment by
    using this script again... e.g.
       ./start-vscode.ps1 file1 file2
    The Windows Shortcut should point to something like this:
    powershell.exe -NoLogo -NonInteractive -WindowStyle Hidden C:\Path\To\This\Script.ps1
    
    Sample of actual shortcut value:
    C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -NoLogo -NonInteractive -WindowStyle Hidden C:\Users\arf41\Workspaces-Personal\beancount\beancount-bundle\scripts\vscode-beancount.ps1
    
     Author: Alex Richard Ford (arf4188@gmail.com)
    Website: http://www.alexrichardford.com
    License: MIT License
#>

# $SCRIPTS_DIR = $PSScriptRoot
# $PROJECT_DIR = Split-Path -Parent $SCRIPTS_DIR
$ProjectDir = "${PSScriptRoot}"
$ProjectName = Split-Path -Leaf "${ProjectDir}"
$DefaultWorkspaceFile = "default.code-workspace"
$WorkspaceFile = "${ProjectName}.code-workspace"

$VSCode = "${env:LOCALAPPDATA}\Programs\Microsoft VS Code\Code.exe"
if (-Not (Test-Path "${VSCode}")) {
    Write-Error "VS Code not found at: ${VSCode}"
    exit 1
}

$CurrentDir = Get-Location

Set-Location "${ProjectDir}"

# if the DefaultWorkspaceFile is present, rename it to be the project workspace file
if (Test-Path "${DefaultWorkspaceFile}") {
    Write-Output "Renaming default VS Code workspace file to: ${WorkspaceFile}"
    Rename-Item ${DefaultWorkspaceFile} ${WorkspaceFile}
}

if (-Not (Test-Path "${WorkspaceFile}")) {
    Write-Error "VS Code workspace file '${WorkspaceFile}' doesn't exist!"
    exit 1
}

# if the user specifies no arguments, then we imply the project workspace file
# else, we pass through the user's arguments
Write-Output "args = ${args}"
$PassedArgs = New-Object System.Collections.ArrayList
if ($args.Count -eq 0) {
    $PassedArgs.Add("${WorkspaceFile}")
}
else {
    $PassedArgs.Add($args)
}
Write-Output "PassedArgs = $(${PassedArgs}|Out-String)"

& $VSCode --verbose `
    --user-data-dir ".\.vscode\user-data\" `
    --extensions-dir ".\.vscode\extensions\" `
    --skip-getting-started `
    --disable-telemetry `
    ${PassedArgs}

Set-Location "${CurrentDir}"
